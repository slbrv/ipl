require "#{File.dirname(__FILE__)}/lw6_3_lib.rb"

print("Input B: "); b = gets.to_i
puts("M(x * sin(x)) with block: #{scale(b, Proc.new{|x| x * Math.sin(x)}, -2.0, 2.0, 50)}")
puts("M(x * sin(x)) with lamda: #{scale(b, lambda{|x| x * Math.sin(x)}, -2.0, 2.0, 50)}")

puts("M(tg(x)) with block: #{scale(b, Proc.new{|x| Math.tan(x)}, -2.0, 2.0, 50)}")
puts("M(tg(x)) with lamda: #{scale(b, lambda{|x| Math.tan(x)}, -2.0, 2.0, 50)}")