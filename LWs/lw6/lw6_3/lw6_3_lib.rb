def maxF(func, from, to, accur)
  dx = (to - from) / accur
  max = 0.0
  y = 0.0
  (0..accur).each do |i|
    y = func.call(from + dx * i)
    max = y > max ? y : max
  end
  return max
end

def scale(varB, func, from, to, accur)
  varB / maxF(func, from, to, accur)
end