def integral(from, to, accur)
  sqr = 0.0
  dx = (to - from) / accur
  x = from
  while x < to
    sqr += Math.tan(x)**2 * dx
    x += dx
  end
  return sqr
end
