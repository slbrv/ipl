require "#{File.dirname(__FILE__)}/lw6_1_lib.rb"
print('Input Accuracy: ')
accur = gets.to_i
val = integral(Math::PI / 4, Math::PI / 3, accur)
puts("Fun calc: #{val}")
ideal = Math.sqrt(3) - Math::PI / 12 - 1
puts("Ideal: #{ideal}")
puts("Inaccuracy: #{(val - ideal).abs}")
