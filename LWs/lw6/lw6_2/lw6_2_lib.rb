def integral(from, to, accur)
  sqr = 0.0
  dx = (to - from) / accur
  (0..accur).each { |i| sqr += Math.tan(from + (dx * i))**2 * dx }
  return sqr
end