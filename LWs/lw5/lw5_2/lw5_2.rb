require "#{File.dirname(__FILE__)}/lw5_2_lib.rb"
lines = Array.new
begin
  print("Input row: ")
  inp = gets.chomp
  lines.push(inp)
end while(not inp.casecmp? "end")
lines.delete_at(lines.size-1)
print("Input width: ")
width = gets.to_i
norm_print(lines, width)