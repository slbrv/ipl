def norm_print(lines, width)
  lines.each do |string|
    strings = string.scan(/\w+/)
    (0..(width-strings.join.size)).each { |w|strings.at(w % (strings.size > 1 ? strings.size - 1 : 1)) << ' '}
    puts(strings.join)
  end
end